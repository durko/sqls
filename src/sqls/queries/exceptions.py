# Copyright 2020-2023 Marko Durkovic.
# SPDX-License-Identifier: Apache-2.0
"""Query exceptions."""


class QueryError(Exception):
    """Query error."""
