.. include:: ../README.rst


.. toctree::
   :caption: Documentation
   :hidden:
   :maxdepth: 2

   topics/transactions
   topics/models
   topics/queries


.. toctree::
   :caption: API
   :hidden:

   api/index


.. toctree::
   :caption: Changes
   :hidden:

   changes


.. toctree::
   :caption: Links
   :hidden:

   Source Code <https://gitlab.com/durko/sqls>
   Issues <https://gitlab.com/durko/sqls/issues>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
